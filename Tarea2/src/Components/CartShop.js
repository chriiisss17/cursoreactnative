import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, TouchableOpacity, View, StyleSheet, Alert } from 'react-native';

const styles= StyleSheet.create({
    container: {
        margin: 10,
        borderRadius: 7,
        backgroundColor: '#27ae60',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 60,
    },
    containerButton: {
        margin: 10,
    },
    button: {
        borderRadius: 7,
        backgroundColor: '#e74c3c',
        margin: 10,
        height: 30,
        width: '30%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: 'white',
    }
})

export default class CartShop extends Component {
    constructor(props){
        super(props);

        this.state = {
            counter: 0,
        };
    }

    static getDerivedStateFromProps = (props,state) =>{

        const { itemsInCart } = props;
        
        return {
            counter: itemsInCart.length,
        }

    }
    
    reset = () => {
        const { itemsInCart, children } = this.props;

        this.setState({counter: itemsInCart.length=0})


    }


    alertCart = () => Alert.alert(
        "Confirmación","¿Quieres eliminar todo el carrito?",
    [
        {text: "Borra todo!!",onPress: this.reset},
        {text: "Me arrepentí"}
    ])

    render() {
        const { counter} = this.state;
        const { itemsInCart, open } = this.props;

        if(itemsInCart.length >0 ){
            return (
                <>
                <View style={styles.container}>
                    <View style={styles.containerButton}>
                        <Text style={styles.text}>Total carrito ({counter})</Text>
                    </View>
                    <TouchableOpacity 
                    style={styles.containerButton,styles.button}>
                        <Text
                        onPress={this.alertCart}
                        style={styles.text}>Vaciar Carrito</Text>
                    </TouchableOpacity>
                </View>
                </>
            )
        }
        return null
    }
}