import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Image, StyleSheet, View, Text, TouchableOpacity, Linking, Alert } from 'react-native'
import {baseUri, dishData, dishTitles} from '../../Data/index'
import CartShop from '../CartShop'

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'gray',
        borderRadius: 7,
        marginBottom: 10,
    },
    imageContainer: {
        height: 100,
    },
    image: {
        borderTopLeftRadius: 7,
        borderTopRightRadius: 7,
        backgroundColor: 'black',
        width: '100%',
        height: '100%',
    },
    textContainer: {
        padding: 2,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 15,
    },
    info: {
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    link: {
        margin: 5,
        color: 'white',
        textShadowColor: 'black',
        textDecorationLine: 'underline', 
    },
    button: {
        backgroundColor: '#9b59b6',
        borderBottomEndRadius: 7,
        borderBottomStartRadius: 7,
        height: 40,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
    },
})

const OpenUrl = ({sourceUrl}) => (
    <TouchableOpacity
    onPress={()=> Linking.openURL(sourceUrl)}
        >
        <Text style={styles.link}>Ver receta</Text>
    </TouchableOpacity>
)



export default class DishCard extends Component {
    constructor(props){
        super(props);
    }

    render() {
        const {selectFood,title, readyInMinutes, servings, image, sourceUrl, style, styleImage} = this.props;
        return (
            <View style={[styles.container,style]}>
                <View style={[styles.imageContainer,styleImage]}>
                    <Image
                        style={styles.image}
                        resizeMode='cover'
                        source={{ uri: `${baseUri}${image}`}}
                    />
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.title}>{title}</Text>
                    <View style={styles.info}>
                        <Text>{`Listo en ${readyInMinutes}`}</Text>
                        <Text>{`Para ${servings} personas`}</Text>
                    </View>
                </View>
                <OpenUrl 
                sourceUrl={sourceUrl}/>
                <TouchableOpacity
                    onPress={selectFood}
                    style={styles.button}
                        >
                    <Text style={styles.buttonText}>Agregar al carro</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
