import React, { Component } from 'react'
import PropTypes from 'prop-types'
import HorizontalScroll from '../HorizontalScroll'
import { Alert, FlatList, StyleSheet, Text, View} from 'react-native'
import {dishData,dishTitles,baseUri} from '../../Data/index'
import DishCard from './DishCard'
import CartShop from '../CartShop'

const styles = StyleSheet.create({
    flatListContainer: {
        backgroundColor: '#2c3e50',
        padding: 10,
        width: '100%',
        flexGrow: 0,
        height: '60%',
    },
    title: {
        color: 'white',
        fontSize: 18,
        textDecorationLine: 'underline',
    },
    flatListContainerPedidos: {
        backgroundColor: '#2c3e50',
        padding: 10,
        width: '100%',
        height: '40%',
    },
    contentText: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: 'white',
    }
})

export default class DishList extends Component {
    constructor(props){
        super(props);

        this.state={
            dishes: dishData,
            selectedDishes: [],
            itemsInCart: [],
            counter: 0,
            open: false,
        };

    }

    selectDishes=(title)=>{ 

        const { dishes } = this.state;

        const filteredDishes = dishes.filter((dish) => dish.cuisine===title);

        this.setState({selectedDishes: filteredDishes});

    }

    selectFood = (filterDish,index) => {
       
        const { itemsInCart, open } = this.state;

        const food = filterDish.find((dish,indice)=>indice==index)

        const newList = [...itemsInCart,food,]

        //Si ya existe la comida, le quita el objeto
        if(itemsInCart.includes(food)){
            newList.pop()
        }//caso contrario lo agrega y lo asigna al estado del componente
        this.setState({itemsInCart: newList})
        
        Alert.alert("!Muy Bien!","Agregado al carrito")

        if(!itemsInCart.length){
            this.setState({open: !open})
        }

        
        
    }

    render() {
        const {selectedDishes, itemsInCart, open} = this.state;

        return (
            <>
            <HorizontalScroll 
                style={{margin: 10}}
                onPress={this.selectDishes}
            />
            <FlatList 
                style={styles.flatListContainer}
                data={selectedDishes}
                keyExtractor={({id})=>id.toString()}
                renderItem={({item: {title, readyInMinutes, servings, image, sourceUrl},index}) =>(
                    <DishCard
                    selectFood={()=>this.selectFood(selectedDishes,index)}
                        title={title}
                        readyInMinutes={readyInMinutes}
                        servings={servings}
                        image={image}
                        sourceUrl={sourceUrl}
                    >
                    </DishCard>)}
                ListEmptyComponent={()=>(
                    <View>
                        <Text style={styles.title}>No hay comida </Text>
                    </View>
                )}
                ListHeaderComponent={()=>(
                    <View>
                        <Text 
                            style={styles.title}>Platos</Text>
                    </View>
                )}
                />
                <FlatList
                    style={styles.flatListContainerPedidos}
                    data={itemsInCart}
                    keyExtractor={({id})=>id.toString()}
                    renderItem={({item: {title, readyInMinutes, servings, image, sourceUrl},index}) =>(
                    <DishCard
                    style={{flexDirection: 'row',justifyContent: 'flex-start',alignItems: 'flex-start',height: 70}}
                    styleImage={{width: '30%',height: 70}}
                            title={title}
                            readyInMinutes={readyInMinutes}
                            servings={servings}
                            image={image}
                            sourceUrl={sourceUrl}
                    >
                    </DishCard>)}
                    ListHeaderComponent={()=>(
                        <View>
                            <Text style={styles.title}>Pedidos</Text>
                        </View>
                    )}
                    ListEmptyComponent={()=>(
                        <View style={styles.contentText}>
                            <Text style={styles.text}>Lista vacía</Text>
                        </View>
                    )}
                />
                <CartShop
                open={open}
                itemsInCart={itemsInCart}
                />
            </>
        )
    }
}


DishList.propTypes = {
    style: PropTypes.shape({}),
    styleImage: PropTypes.shape({}),
}

DishList.defaultProps = {
    style: {},
    styleImage: {},
}