
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  View
} from 'react-native';
import CartShop from './Components/CartShop';
import DishCard from './Components/Dish/DishCard';
import DishList from './Components/Dish/DishList';

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#2c3e50',
  },
})

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.safeArea}>
        <DishList>
        </DishList>
      </SafeAreaView>
    </>
  );
};


export default App;
