import React, { Component, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Text, TouchableOpacity, View, StyleSheet, Alert } from 'react-native';

const styles= StyleSheet.create({
    container: {
        margin: 10,
        borderRadius: 7,
        backgroundColor: '#27ae60',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 60,
    },
    containerButton: {
        margin: 10,
    },
    button: {
        borderRadius: 7,
        backgroundColor: '#e74c3c',
        margin: 10,
        height: 30,
        width: '30%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: 'white',
    }
})

const CartShop = ({ itemsInCart }) => {

    const [counter, setCounter] = useState(0)

    const initCounter = () => setCounter(itemsInCart.length)

    useEffect(()=>initCounter())
    
    const reset = () => setCounter(itemsInCart.length=0)


    const alertCart = () => Alert.alert(
        "Confirmación","¿Quieres eliminar todo el carrito?",
    [
        {text: "Borra todo!!",onPress: reset},
        {text: "Me arrepentí"}
    ])

    if(itemsInCart.length >0 ){
        return(
            <View style={styles.container}>
                <View style={styles.containerButton}>
                    <Text style={styles.text}>Total carrito ({counter})</Text>
                </View>
                <TouchableOpacity 
                style={styles.containerButton,styles.button}>
                    <Text
                    onPress={alertCart}
                    style={styles.text}>Vaciar Carrito</Text>
                </TouchableOpacity>
            </View>
        )
    }
    return null
}
export default CartShop