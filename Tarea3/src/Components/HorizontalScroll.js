import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FlatList, TouchableOpacity,Text, StyleSheet, View } from 'react-native'
import { dishTitles,baseUri,dishData } from '../Data/index'

const styles= StyleSheet.create({
    container: {
        margin: 5,
        backgroundColor: '#27ae60',
    },
    button: {
        margin: 5,
    },
    text: {
        color: 'white',
        fontSize: 16,
    }
})

export default class HorizontalScroll extends Component {
    render() {
        const {style, item, onPress} = this.props;
        return (
        <View 
            style={styles.container}>
            <FlatList
            horizontal
            data={item || dishTitles}
            keyExtractor={({id}) => id.toString()}
            renderItem={({item : {id,title}})=>(
                <TouchableOpacity 
                 onPress={() => onPress(title)}
                   style={styles.button} 
                    >
                    <Text
                    style={styles.text}
                        >{title}</Text>
                </TouchableOpacity>
            )}
            />
            </View>
        )
    }
}

HorizontalScroll.propTypes = {
    style: PropTypes.shape({}),
    items: PropTypes.array,
    onPress: PropTypes.func,
}

HorizontalScroll.defaultProps = {
    style: {},
    items: [],
    onPress: ()=>{},
}