import dishData from './dishData.json';

const dishTitles = [
    {id: 1,title: 'China'},
    {id: 2,title: 'Francesa'},
    {id: 3,title: 'Italiana'},
    {id: 4,title: 'América Latina'},
];

const baseUri = 'https://spoonacular.com/recipeImages/';

export {dishData,dishTitles, baseUri};